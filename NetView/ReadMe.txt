
Network View - version 1.20

Displays a list of all machines visible in the specified domain, based on role.


The syntax of this command is:

NetView.exe [/NTW] [/NTS] [/PDC] [/BDC] [/PRINT] [/RAS] [/SQL] [/TIME] [/TS]
   [/9x] [/HEX:0xXXXXXXXX] [/DOMAIN:domain] [/T] [/B] [/TABS]

 Multiple switches can be specified, separate each with a space.

 /NTW will include NT/2000/XP workstations.
 /NTS will include NT/2000/.NET servers.
 /PDC will include NT/2000/.NET primary domain controllers.
 /BDC will include NT backup domain controllers.
 /PRINT will include Print queue servers.
 /RAS will include Remote Access Servers.
 /SQL will include Microsoft SQL Servers.
 /TIME will include all Time sources.
 /TS will include all Terminal Servers.
 /9x will include Windows 95/98/ME systems. (Only if sharing has been enabled.)
 /HEX: allows you to specify your own bit mask.  Use /HEX: /? for more details.

 /DOMAIN: allows you to specify a different domain or workgroup.
 /T also displays the primary machine type (Server, Workstation, PDC, etc.)
 /B displays the output in bare format (useful when scripting)
 /TABS separates columns with tabs instead of spaces (useful when importing)


 An argument of /? or -? displays this syntax and always returns 1.
 A successful completion will return 0.


 Copyright 2000-2001  Marty List, OptimumX@usa.net

==================================================================

System Requirements:
	Windows XP; Windows 2000; Windows NT
	NETBIOS and the Computer Browser service enabled


Revision History:
	1.20 	08/16/2001
	Added support for Terminal Servers (/TS)
	Altered /HEX: to override all other filters

	1.10 	08/16/2000
	Added support for /T, /B and /TABS

	1.00 	01/12/2000
	Initial release.
