﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NetworkSearch;

namespace WhereAmILoggedIn
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly BackgroundWorker worker;
        private NetworkSearch.Machines searchResults;
        NetworkSearch.Search search;

        public MainWindow()
        {
            search = new NetworkSearch.Search();
            InitializeComponent();
            worker = this.FindResource("backgroundWorker") as BackgroundWorker;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!worker.IsBusy)
            {
                this.Cursor = Cursors.Wait;
                //worker.RunWorkerAsync(
                Dictionary<string, string> args = new Dictionary<string, string>();
                args.Add("searchTerm", txt_UserName.Text);
                worker.RunWorkerAsync(args);
                btn_search.Content = "Cancel";
            }
            else
            {
                this.Cursor = Cursors.Arrow;
                btn_search.Content = "Search";
                search.cancel();
                worker.CancelAsync();
            }
        }

        private void BackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Dictionary<string, string> args = (Dictionary<string, string>)e.Argument;
            searchResults = search.search(args["searchTerm"]);
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
            btn_search.Content = "Search";
            dg_machines.ItemsSource = search.Results;
            dg_all.DataContext = search.Cache.Select( s=> new {
                MachineName = s.MachineName,
                userNames = s.userNames.DefaultIfEmpty()
            });
            
            Names names = new Names();
            SearchResultViewName name = new SearchResultViewName("-");
            names.Add(name);

            dg_all.ItemsSource = search
                .Cache
                .Select(s => new
                {
                    MachineName = String.IsNullOrEmpty(s.MachineName) ? "ERROR" : s.MachineName,
                    Names = s.userNames == null ? names : s.userNames
                })
                .SelectMany(
                    m => m.Names,
                    (o, m) => new
                    {
                        MachineName = o.MachineName,
                        Name = m.name
                    }
                );
            
        }

        /// <summary>
        /// Handle when progress changes on background worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        
        {
            
        }

        private void BackgroundWorker_Disposed_1(object sender, EventArgs e)
        {
            
        }

      
    }
}
