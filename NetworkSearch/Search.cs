﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetworkSearch
{
    public class Search 
    {
        public bool ShowHistory{ get; set; }
        public string UserName{ get; set; }
        public bool ShowLocalUsers { get; set; }
        public Machines Results { get { return _results; } }
        public Machines Cache{get{return cache;}}
        
        private List<string> _machineNames;
        private List<string> _machineNamesResults;
        private Machines _results;
        private Process process;
        private Machines cache;
        private bool hasMachineNames = false;
        private bool hasResults = false;

        public Search(bool includeOldSessions = false, bool showLocalOnlyUsers = false)
        {
            cache = new Machines();
            ShowHistory = includeOldSessions;
            ShowLocalUsers = showLocalOnlyUsers;
        }

        public Search(string userName, bool includeOldSessions = false, bool showLocalOnlyUsers = false)
        {
            cache = new Machines();
            UserName = userName;
            ShowHistory = includeOldSessions;
            ShowLocalUsers = showLocalOnlyUsers;
        }

        public Machines search(string userName)
        {
            UserName = userName;
            return search();
        }

        public Machines search()
        {
            //Get A list of the machines
            getMachineList();
            
            // Make sure there is a machine list to search against
            if (hasMachineNames)
            {
                getLoggedInSessions();
            }
            
            return _results;
        }

        public void checkResultsAgain()
        {
            if (hasMachineNames)
            {
                getLoggedInSessions();
            }
        }

        public void cancel(){
            process.Kill();
        }

        private void getMachineList()
        {
            // If it already has a list, return
            if (hasMachineNames){
                return;
            }
            
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\NetView.exe";
            //info.Arguments = "";
            info.UseShellExecute = false;
            info.CreateNoWindow = true;
            info.RedirectStandardOutput = true;
            
            // Setup the process
            process = new Process();
            process.StartInfo = info;
            
            process.Start();

            string netViewOutput = process.StandardOutput.ReadToEnd();

            process.WaitForExit();

            if (process.ExitCode != 0)
            {
                return;
            }

            _machineNames = ResultParsers.parseNetViewResultsAsMachineNameList(netViewOutput);

            _machineNames.ForEach( m => {
                cache.Add(new SearchResultView(m));
            });

            hasMachineNames = true;

            return;
        }
        
        private void getLoggedInSessions()
        {
            if (hasResults)
            {
                return;
                //getSessionsFromCache();
            }
            else
            {
                getSessionsFromNetUser();
            }

            return;
        }

        private void getSessionsFromCache()
        {
            _results = new Machines();

            /*foreach ( SearchResultView machine in cache)
            {
                foreach (SearchResultViewName name in machine.userNames)
                {
                    if (name.name.IndexOf(UserName) > -1)
                    {
                        _results.Add(new SearchResultView(machine.MachineName));
                        break;
                    }
                }
            }*/
        }

        private void getSessionsFromNetUser()
        {
            _results = new Machines();

            foreach (string machineName in _machineNames)
            {
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\NetUsers.exe";
                info.Arguments = machineName;
                info.UseShellExecute = false;
                info.CreateNoWindow = true;
                info.RedirectStandardOutput = true;

                // Setup the process
                process = new Process();
                process.StartInfo = info;

                process.Start();

                string userViewOutput = process.StandardOutput.ReadToEnd();

                process.WaitForExit();

                if (process.ExitCode != 0 && process.ExitCode != 53)
                {
                    break;
                }

                List<string> userNames = ResultParsers.parseNetUsersResultsAsUserNameList(userViewOutput);

                // get the current machine in the loop
                var machineMatch = cache.Where(w => w.MachineName == machineName);

                // If there is more than 1 machine with the same name throw an error.
                if (machineMatch.Count() != 1)
                {
                    throw new Exception(String.Format("It appears that the machine name '{0}' is a duplicate.", machineName));
                }

                // There should only be one machine matched at this point, but just as a double check query single
                SearchResultView machine = ((SearchResultView)machineMatch.Single());

                // Set up a new Names list in the user names param of the machine
                machine.userNames = new Names();

                foreach (string name in userNames)
                {
                    // Add the current user name to the current machine.
                    machine.userNames.Add(new SearchResultViewName(name));

                    // This will set up the resulting list of machine names for the user search
                    if (name.IndexOf(UserName) > -1)
                    {
                        _results.Add(new SearchResultView(machineName));
                        break;
                    }
                }

            }

            hasResults = true;
        }
    }

    
    /// <summary>
    /// Results View Object
    /// </summary>
    public class SearchResultView
    {
        public String MachineName { get; set; }
        public Names userNames { get; set; }

        public SearchResultView(string machineName)
        {
            MachineName = machineName;
        }
    }

    public class SearchResultViewName
    {
        public String name { get; set; }

        public SearchResultViewName(string userName)
        {
            name = userName;
        }
    }

    public class Machines : System.Collections.ObjectModel.ObservableCollection<SearchResultView> { }
    public class Names : System.Collections.ObjectModel.ObservableCollection<SearchResultViewName> { }
}
