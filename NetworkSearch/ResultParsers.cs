﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetworkSearch
{
    public class ResultParsers
    {
        /// <summary>
        /// Parse Results from NetView output.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<string> parseNetViewResultsAsMachineNameList( string input ) {
            List<String> ret = new List<string>();
            
            Regex match = new Regex(@"\\\\[^\s]+");
            MatchCollection machineNames = match.Matches(input);

            for( int i = 0; i < machineNames.Count; i++){
                ret.Add((string)machineNames[i].Value);
            }

            return ret;
        }

        /// <summary>
        /// Parse Results form UserView output.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<string> parseNetUsersResultsAsUserNameList(string input)
        {
            List<String> ret = new List<string>();

            Regex match = new Regex(@"[^\s]+\\[^\s]+");
            MatchCollection userNames = match.Matches(input);

            for (int i = 0; i < userNames.Count; i++)
            {
                ret.Add(userNames[i].Value.ToString());
            }
            
            return ret;
        }
    }
}
